import math
import sklearn
import mlkit
import xmltodict
import collections
from normalizer.main import normalize_job_title
from normalizer.pipelines.pipelinemanager import PipelineManager


_predictor = None
_csvContent = ""


def main():
    _init()
    with open("rome.xml", mode="rb") as fp:
        xmltodict.parse(fp, item_depth=2, item_callback=_handle_function)
    with open("rome_taxonomy_idv2_evaluation.csv", "w",
              encoding="iso-8859-15") as fw:
        fw.write(_csvContent)
    return


def _handle_function(path, function):
    global _csvContent
    fname = function["bloc_code_rome"]["intitule"]
    for syn in function["appellation"]["item"]:
        if type(syn) is not collections.OrderedDict:
            continue
        _csvContent += "%s;%s;%s\n" % (fname, syn["libelle_court"],
                                        str(syn["libelle_court"]))
    return True


def _init():
    global _predictor
    s = mlkit.PredictionService(
        {'required_predictors': ['job_title_embedding']})
    _predictor = s.get_predictor('job_title_embedding')
    PipelineManager.pre_fork_initialize()


def _tell_distance(jt1, jt2):
    assert _predictor is not None
    vec1, _, _ = _predictor.predict({'job_title': jt1})
    vec2, _, _ = _predictor.predict({'job_title': jt2})
    dist = sklearn.metrics.pairwise.pairwise_distances(
        vec1, vec2, metric='correlation')[0][0]
    if math.isfinite(dist):
        dist = round(dist, 3)
    else:
        dist = None
    return dist


if __name__ == "__main__":
    main()
